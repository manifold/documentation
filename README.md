# Cryzen documentation

Cryzen trading platform documentation and example code.

# Manual

#General code structure

Every algorithm in Cryzen code studio consits of the definitions of two system functions ```setup(context)``` and ```run(data)```. 

## Setup function
Setup function is called at the start of the algorithm and sets up the inviroment in which the main code is executed.
It allows user to pick the data streams, and, in case of backtesting, desired initial conditions.

```python
def setup(context):
    context.attribute_1 = ...
    context.attribute_2 = ...
    ...
```
The following attributes **must** be defined both for live and backtest mode
1. ```context.exchanges = {exchange_name: symbols_list}``` - defines the exchanges and cryptocurrency pairs that the user wants to have access to. 
Up to two cryptocurrency pairs are supported at the moment. Example values:
    *  `exchange_name = 'binance'`
    *  `symbols_list = ['BTC/USDT']`

The following attributes **must** be defined for backtest mode only
* ```context.start = '2018-10-20T10:00:00 UTC+0'``` the start of backtesting period
* ```context.end = '2018-10-21T12:00:00 UTC+0'``` the end of backtesting period
* ```context.accounts = { account_name: {'id': exchange_name, 'balance': {currency1 : value, currency2: value}}}``` Example values:
    * sds

## Run function